############################################################
## Copyright 2021-2022
## Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
############################################################

define WELCOME_MESSAGE
-------------------------------------------------------------------------------
-- Flexible Accelerators Library (FAL)                                       --
-- ECASLab 2021-2022                                                         --
-------------------------------------------------------------------------------
Authors:
- MSc.-Ing. Luis G. Leon Vega <lleon95@estudiantec.cr>
- Dr.-Ing. Jorge Castro Godinez <jocastro@tec.ac.cr>

Contributors:
- Alejandro Rodriguez Figueroa <alejandrorf@estudiantec.cr>
- Eduardo Salazar Villalobos <eduarsalazar@estudiantec.cr>
- Esteban Campos Granados <estebandcg1999@estudiantec.cr>
- Erick A. Obregon Fonseca <erickof@estudiantec.cr>
- Fabricio Elizondo Fernandez <faelizondo@estudiantec.cr>
- David Cordero Chavarría <dacoch215@estudiantec.cr>

This project is licensed under Apache v2.

Building System Version: v$(VERSION)
endef
