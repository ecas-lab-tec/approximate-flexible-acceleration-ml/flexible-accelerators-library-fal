/*
 * Copyright 2022
 * Author: Luis G. Leon-Vega <lleon95@estudiantec.cr>
 */

#pragma once

#include "enums.hpp"
#include "macros.hpp"
#include "type.hpp"
